function isinvp (el) {
    var rect = el.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}

function check_and_unlike(tweet) {
    
    //tweet_in_vp = isinvp(tweet);
    
    //if (tweet_in_vp) {
        
        // Get time of tweet
        try{ twtime = tweet.querySelector("time").dateTime; }
        catch { twtime = new Date(); twtime = twtime.getDate(); }
        // find a-parent which has tweet url

        // Get timestamp 90 days ago
        t90d = new Date();
        t90d.setDate(t90d.getDate() - 90);
        
        // unlike only if tweet is older than 90 days
        if ((Date.parse(t90d) - Date.parse(twtime)) > 0) {
            
            // get ID of Tweet (href used as ID)
            tw_id = tweet.querySelector("time").parentNode.getAttribute("href");
            
            if (global_tweet_register.indexOf(tw_id) < 0) {
            
                // add Tweet ID to register
                global_tweet_register.push(tw_id);
            
                // trigger like button
                // (older liked tweets are not marked as "liked" anymore)
                try {tw_lkb = tweet.querySelector("div[data-testid=like]"); }
                finally { if (tw_lkb != null) tw_lkb.click(); }

                // trigger unlike button after short wait
                setTimeout(function() {
                    try { tw_ukb = tweet.querySelector("div[data-testid=unlike]"); }
                    finally { if (tw_ukb != null) tw_ukb.click(); }
                }, 1500);
                
                // print output
                console.log("unliked: " + tw_id + " @ " + twtime);
                global_like_deleted = true;
            }
        }
    //}
}

function get_tweets() {
    // find all tweets in the current view / scroll section
    tweets = document.querySelectorAll("article");
    // run the check-and-unlike procedure on all of them individually
    for (let i = 0; i < tweets.length; i++) {
        setTimeout(function() { check_and_unlike(tweets[i]); }, 250);
    }
}

function daemon() {
    // run procedure
    get_tweets();
    if (global_like_deleted) {
        timeout = (1000 * 60 * 5); // 5 minutes
        global_like_deleted = false;
    } else {
        timeout = 500;
    }
    // scroll page to the very bottom (load new tweets)
    //window.scrollByLines(15);
    window.scrollByPages(1);
    // after wait, recall yourself!
    setTimeout(function() { daemon(); }, timeout);
}

let global_tweet_register = [];
var global_like_deleted = false;
daemon();
